import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { LoginModule } from './login/login.module';

import { AppComponent } from './app.component';

import { rootRouterConfig } from './app.routes';

@NgModule({
	declarations: [
		AppComponent,
	],
	imports: [
		BrowserModule,
		LoginModule,
		HttpClientModule,
		RouterModule.forRoot(rootRouterConfig)
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
