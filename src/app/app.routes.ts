import { Routes } from '@angular/router';

import { LoginFormComponent } from './login/components/login-form/login-form.component';

export const rootRouterConfig: Routes = [{
	path: '',
	redirectTo: 'auth',
	children: [{
		path: 'auth',
		component: LoginFormComponent,
	}]	
}];

