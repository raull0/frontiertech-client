import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginFormComponent } from './components/login-form/login-form.component';

@NgModule({
	declarations: [
		LoginFormComponent
	],
	imports: [
	],
	providers: [

	],
})
export class LoginModule { }