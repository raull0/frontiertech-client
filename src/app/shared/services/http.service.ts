import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpHeaders, HttpClient, HttpResponse, HttpParams, HttpResponseBase } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {
	private baseUrl:string = "http://localhost:4200/api";

	constructor(private http: HttpClient) { }

	public get(url:string): Observable<any> {
		return this.http.get(this.baseUrl + url).share().map((response:Response) => {
			return response.json();
		});
	}

	public post(url:string, data:any): Observable<any> {
		return this.http.post(this.baseUrl + url, data).share().map((response:Response) => {
			return response.json();
		});
	}

	public put(url:string, data:any): Observable<any> {
		return this.http.put(this.baseUrl + url, data).share().map((response:Response) => {
			return response.json();
		});
	}

	public delete(url:string):  Observable<any> {
		return this.http.delete(this.baseUrl + url).share().map((response:Response) => {
			return response.json();
		});
	}
}